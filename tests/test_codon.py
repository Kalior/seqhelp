import os
from collections import defaultdict
from pathlib import Path

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

from seqhelp import codon


def test_generate_relative_adaptiveness_table():
    first_codon = [c[0] for c in codon._SYNONYMOUS_CODONS.values()]
    rscu_table = defaultdict(lambda: 0.5)
    for c in first_codon:
        rscu_table[c] = 1.5
    rat = codon.generate_relative_adaptiveness_table(rscu_table)

    for c in first_codon:
        assert rat[c] == 1


def test_generate_rscu_table():
    first_codon = [c[0] for c in codon._SYNONYMOUS_CODONS.values()]
    codon_count = defaultdict(lambda: 0.5)
    for c in first_codon:
        codon_count[c] = 2

    rscu_table = codon.generate_rscu_table(codon_count)

    for synonymous_codons in codon._SYNONYMOUS_CODONS.values():
        n_synonymous_codons = len(synonymous_codons)
        all_count = 2 + (n_synonymous_codons - 1) * 0.5
        denominator = all_count / n_synonymous_codons
        for i, c in enumerate(synonymous_codons):
            if i == 0:
                assert rscu_table[c] == 2 / denominator
            else:
                assert rscu_table[c] == 0.5 / denominator


def test_count_codons():
    sequence = "GATGATGATGATTGT"
    record = SeqRecord(Seq(sequence))
    codon_count = codon.count_codons([record])

    assert codon_count["GAT"] == 4
    assert codon_count["TGT"] == 1
    # Non seen codons are assigned a value of 0.5
    assert codon_count["TGG"] == 0.5


def test_cai_for_gene():
    reference_record = SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE")
    codon_count = codon.count_codons([reference_record])
    rscu_table = codon.generate_rscu_table(codon_count)
    rat = codon.generate_relative_adaptiveness_table(rscu_table)

    test_record = SeqRecord(Seq("TGTGATGATGATGAT"), id="TEST_GOOD")

    good_cai = codon.codon_adaptation_index(rat, [test_record])
    assert good_cai == 1.0

    test_bad_record = SeqRecord(Seq("GACGACGACGACTGC"), id="TEST_BAD")

    cai = codon.codon_adaptation_index(rat, [test_bad_record])
    assert cai < 0.2


def test_cache_codon_count(tmpdir_factory, monkeypatch):
    def mockreturn(*args, **kwargs):
        return [SeqRecord(Seq("GATGATGATGATTGT"), id="AF192")]

    monkeypatch.setattr("Bio.SeqIO.parse", mockreturn)

    tmp_cache = tmpdir_factory.mktemp("cache")
    dir_ = Path(os.path.realpath(__file__)).parent
    codon.cache_codon_count("AF192", dir_ / "AF379637.1.fa", Path(str(tmp_cache)))

    assert os.path.isfile(tmp_cache.join("AF192-codons.json"))
