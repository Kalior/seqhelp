import os
from pathlib import Path

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from pytest import approx

from seqhelp import codon_pair, codon


def test_generate_codon_pair_score_table():
    test_record = SeqRecord(Seq("GACGACGACGACTGC"), id="TEST")
    codon_pair_count = codon_pair.count_codon_pairs([test_record])
    codon_count = codon.count_codons([test_record])
    cpst = codon_pair.generate_codon_pair_score_table(codon_count, codon_pair_count)

    assert cpst["GACGAC"] == approx(0.235566)
    assert cpst["GACTGC"] == approx(0.523248)
    assert cpst["GATTGT"] == 0


def test_count_codon_pairs():
    test_record = SeqRecord(Seq("GACGACGACGACTGC"), id="TEST")
    codon_pair_count = codon_pair.count_codon_pairs([test_record])

    assert codon_pair_count["GACGAC"] == 3
    assert codon_pair_count["GACTGC"] == 1
    assert codon_pair_count["ACGACG"] == 0


def test_codon_pair_bias_for_gene():
    reference_record = SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE")
    codon_count = codon.count_codons([reference_record])
    codon_pair_count = codon_pair.count_codon_pairs([reference_record])
    cpst = codon_pair.generate_codon_pair_score_table(codon_count, codon_pair_count)

    test_record = SeqRecord(Seq("GATGATGATGATTGT"), id="TEST_GOOD")

    good_cai = codon_pair.codon_pair_bias(cpst, [test_record])
    assert good_cai == approx(0.409982)

    test_bad_record = SeqRecord(Seq("GACTGCGACTGTGAC"), id="TEST_BAD")

    cai = codon_pair.codon_pair_bias(cpst, [test_bad_record])
    assert cai == 0


def test_cache_codon_pair_count(tmpdir_factory, monkeypatch):
    def mockreturn(*args, **kwargs):
        return [SeqRecord(Seq("GACGACGACGACTGC"), id="AF192")]

    monkeypatch.setattr("Bio.SeqIO.parse", mockreturn)

    tmp_cache = tmpdir_factory.mktemp("cache")
    dir_ = Path(os.path.realpath(__file__)).parent
    codon_pair.cache_codon_pair_count("AF192", dir_ / Path("AF379637.1.fa"), Path(str(tmp_cache)))

    assert os.path.isfile(tmp_cache.join("AF192-codon-pairs.json"))
