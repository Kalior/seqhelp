import os
from collections import defaultdict
from pathlib import Path

import numpy as np
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from pytest import approx

from seqhelp import gc_content


def test_gc_for_gene(tmpdir_factory, monkeypatch):
    tmp_cache = tmpdir_factory.mktemp("cache")
    reference_gc = _gc_train(
        SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE"), monkeypatch, tmp_cache
    )

    assert reference_gc[0] == 4
    assert reference_gc[1] == 0
    assert reference_gc[2] == 5
    assert reference_gc[3] == 6


def test_gc_diff(tmpdir_factory, monkeypatch):
    tmp_cache = tmpdir_factory.mktemp("cache")
    reference_gc = _gc_train(
        SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE"), monkeypatch, tmp_cache
    )
    query_gc = _gc_train(
        SeqRecord(Seq("CCCAAACCCAACCAAGTGTGCCAACCAACCA"), id="QUERY"), monkeypatch, tmp_cache
    )

    assert gc_content.get_gc_diff(reference_gc, query_gc) == approx(0.2725433950861321)


def test_cache_dinucleotides_count(tmpdir_factory, monkeypatch):
    tmp_cache = tmpdir_factory.mktemp("cache")

    _gc_train(SeqRecord(Seq("GATGATGATGATTGT"), id="AF192"), monkeypatch, tmp_cache)

    assert os.path.isfile(tmp_cache.join("AF192-gc.json"))


def _gc_train(record: SeqRecord, monkeypatch, tmp_cache) -> np.ndarray:
    def mockreturn(*args, **kwargs):
        return [record]

    monkeypatch.setattr("Bio.SeqIO.parse", mockreturn)

    dir_ = Path(os.path.realpath(__file__)).parent
    return gc_content.cache_gc_content(record.id, dir_ / "AF379637.1.fa", Path(str(tmp_cache)))
