import os
from collections import defaultdict
from pathlib import Path

import numpy as np
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from pytest import approx

from seqhelp import dinucleotides

from .test_gc import _gc_train


def test_dinucleotides_for_gene(tmpdir_factory, monkeypatch):
    tmp_cache = tmpdir_factory.mktemp("cache")
    counts = dinucleotides._count_dinucleotides(
        [SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE")]
    )

    assert counts["GA"] == 4
    assert counts["AT"] == 4
    assert counts["TG"] == 4
    assert counts["TT"] == 1
    assert counts["GT"] == 1
    present_dinucleotides = ["GA", "AT", "TG", "TT", "GT"]
    non_present = [
        din for din in dinucleotides.DINUCLEOTIDES if din not in present_dinucleotides
    ]
    for din in non_present:
        assert counts[din] == 0


def test_odds_ratio(tmpdir_factory, monkeypatch):
    tmp_cache = tmpdir_factory.mktemp("cache")
    reference_gc = _gc_train(
        SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE"), monkeypatch, tmp_cache
    )
    query_gc = _gc_train(
        SeqRecord(Seq("TGTGATGATGATGAT"), id="QUERY"), monkeypatch, tmp_cache
    )

    reference_dinucleotides = _dinucleotide_train(
        SeqRecord(Seq("GATGATGATGATTGT"), id="REFERENCE"), monkeypatch, tmp_cache
    )
    query_dinucleotides = _dinucleotide_train(
        SeqRecord(Seq("TGTGATGATGATGAT"), id="QUERY"), monkeypatch, tmp_cache
    )

    assert dinucleotides.dinucleotide_odds_ratio_cosine_distance(
        reference_dinucleotides, reference_gc, query_dinucleotides, query_gc
    ) == approx(0.004490262386924182)


def test_cache_dinucleotides_count(tmpdir_factory, monkeypatch):
    tmp_cache = tmpdir_factory.mktemp("cache")

    _dinucleotide_train(
        SeqRecord(Seq("GATGATGATGATTGT"), id="AF192"), monkeypatch, tmp_cache
    )

    assert os.path.isfile(tmp_cache.join("AF192-dinucleotides.json"))


def _dinucleotide_train(record: SeqRecord, monkeypatch, tmp_cache) -> np.ndarray:
    def mockreturn(*args, **kwargs):
        return [record]

    monkeypatch.setattr("Bio.SeqIO.parse", mockreturn)

    dir_ = Path(os.path.realpath(__file__)).parent
    return dinucleotides.cache_dinucleotides(
        record.id, dir_ / "AF379637.1.fa", Path(str(tmp_cache))
    )
