# Welcome to the SeqHelp documentation

SeqHelp is a collection of functions to help with the retrieval and analysis of sequences, primarily from NCBI's nucleotide db. Also includes numba-accelerated computation of gc, dinucleotide, codon, and codon-pair correlation. 

## Sequences 
::: seqhelp.sequences



